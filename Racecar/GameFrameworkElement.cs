﻿//-----------------------------------------------------------------------
// <copyright file="GameFrameworkElement.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Represents the graphical container of the game
    /// </summary>
    public class GameFrameworkElement : FrameworkElement
    {
        /// <summary>
        /// Game instance
        /// </summary>
        private Game game;

        /// <summary>
        /// Car instance
        /// </summary>
        private Car car;

        /// <summary>
        /// Racetrack instance
        /// </summary>
        private Racetrack raceTrack;

        /// <summary>
        /// A timer for handling frames
        /// </summary>
        private DispatcherTimer gameLoopTimer = new DispatcherTimer();

        /// <summary>
        /// A timer for timing key press event handlers
        /// </summary>
        private DispatcherTimer keydownEventTimer = new DispatcherTimer();

        /// <summary>
        /// Keeps track of pressed keys
        /// </summary>
        private List<System.Windows.Input.Key> pressedKeys;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameFrameworkElement"/> class.
        /// </summary>
        public GameFrameworkElement()
        {
            this.pressedKeys = new List<System.Windows.Input.Key>();

            this.Loaded += this.GameFrameworkElement_Loaded;
            this.KeyDown += this.GameFrameworkElement_KeyDown;
            this.KeyUp += this.GameFrameworkElement_KeyUp;
        }

        /// <summary>
        /// Override OnRender method to provide custom rendering
        /// </summary>
        /// <param name="drawingContext">drawingContext instance</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.game != null)
            {
                drawingContext.DrawGeometry(Brushes.Pink, null, this.raceTrack.Body);
                drawingContext.DrawGeometry(Brushes.Black, null, this.car.Body);
            }
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event args</param>
        private void GameFrameworkElement_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.pressedKeys.Remove(e.Key);

            if (this.pressedKeys.Count == 0 && this.keydownEventTimer.IsEnabled)
            {
                this.keydownEventTimer.Stop();
            }
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event args</param>
        private void GameFrameworkElement_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (!this.pressedKeys.Contains(e.Key))
            {
                this.pressedKeys.Add(e.Key);
            }

            if (this.pressedKeys.Count == 1 && (!this.keydownEventTimer.IsEnabled))
            {
                this.keydownEventTimer.Start();
            }
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event args</param>
        private void CustomKeydownHandler(object sender, EventArgs e)
        {
            if (this.pressedKeys.Contains(System.Windows.Input.Key.Left))
            {
                this.car.Rotate(-5);
            }

            if (this.pressedKeys.Contains(System.Windows.Input.Key.Right))
            {
                this.car.Rotate(5);
            }

            if (this.pressedKeys.Contains(System.Windows.Input.Key.Up))
            {
                this.car.Throttle();
            }

            if (this.pressedKeys.Contains(System.Windows.Input.Key.Down))
            {
                this.car.Brake();
            }
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event args</param>
        private void GameFrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            this.raceTrack = new PathBasedRacetrack(this.ActualWidth / 2, this.ActualHeight / 2);

            StartingPosition pos = this.raceTrack.GetStartingPosition();

            this.car = new Car(pos.X, pos.Y, pos.Angle);

            this.game = new Game(this.car, this.raceTrack);

            this.InvalidateVisual();

            // make it focusable
            this.Focusable = true;

            // focus, so user events can be handled
            this.Focus();

            this.gameLoopTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            this.gameLoopTimer.Tick += this.Timer_Tick;
            this.gameLoopTimer.Start();

            this.keydownEventTimer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            this.keydownEventTimer.Tick += this.CustomKeydownHandler;
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">event args</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.game.Step();

            // trigger re-draw
            this.InvalidateVisual();
        }
    }
}
