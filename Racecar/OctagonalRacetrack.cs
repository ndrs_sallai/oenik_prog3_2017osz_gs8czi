﻿//-----------------------------------------------------------------------
// <copyright file="OctagonalRacetrack.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represents a racetrack made of 8 rectangles arranged in a circular position
    /// </summary>
    public class OctagonalRacetrack : Racetrack
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OctagonalRacetrack" /> class.
        /// </summary>
        /// <param name="centerX">center x coordinate</param>
        /// <param name="centerY">center y coordinate</param>
        public OctagonalRacetrack(double centerX, double centerY)
        {
            Geometry combined = null;

            int numberOfRectangles = 8;
            int sectionWidth = 100;
            int sectionHeight = 50;

            for (int i = 0; i < numberOfRectangles; i++)
            {
                Geometry other = new RectangleGeometry(new Rect(centerX - (sectionWidth / 2), centerY - 120, sectionWidth, sectionHeight));

                other.Transform = new RotateTransform(360 / numberOfRectangles * (i + 1), centerX, centerY);

                if (combined != null)
                { 
                    combined = Geometry.Combine(combined, other, GeometryCombineMode.Union, null);
                }
                else
                {
                    combined = other;
                }
            }

            this.Body = combined;
        }

        /// <summary>
        /// Returns the starting position
        /// </summary>
        /// <returns>starting position</returns>
        public override StartingPosition GetStartingPosition()
        {
            return new StartingPosition()
            {
                X = 50,
                Y = 50,
                Angle = 180,
            };
        }
    }
}
