﻿//-----------------------------------------------------------------------
// <copyright file="Game.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the game itself
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="car">the player's car</param>
        /// <param name="racetrack">the racetrack</param>
        public Game(Car car, Racetrack racetrack)
        {
            this.Car = car;
            this.Racetrack = racetrack;
        }

        /// <summary>
        /// Gets the car instance
        /// </summary>
        public Car Car { get; private set; }

        /// <summary>
        /// Gets the racetrack instance
        /// </summary>
        public Racetrack Racetrack { get; private set; }

        /// <summary>
        /// Advances the game state
        /// </summary>
        public void Step()
        {
            this.Car.UpdatePosition();

            if (this.Car.SticksOutOf(this.Racetrack))
            {
                this.Car.Stop();
            }
        }
    }
}
