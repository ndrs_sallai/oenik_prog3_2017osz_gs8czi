﻿//-----------------------------------------------------------------------
// <copyright file="Racetrack.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Represents a racetrack
    /// </summary>
    public abstract class Racetrack : GameObject
    {
        /// <summary>
        /// Abstract method: children must implement a method that returns the starting position
        /// </summary>
        /// <returns>starting position</returns>
        public abstract StartingPosition GetStartingPosition();
    }
}
