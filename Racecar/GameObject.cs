﻿//-----------------------------------------------------------------------
// <copyright file="GameObject.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Base class for UI elements in the game
    /// </summary>
    public class GameObject
    {
        /// <summary>
        /// Gets or sets body
        /// </summary>
        public Geometry Body { get; protected set; }

        /// <summary>
        /// Checks if this object collides with the other one
        /// </summary>
        /// <param name="other">the other object, surprisingly</param>
        /// <returns>if collides with other</returns>
        public bool CollidesWith(GameObject other)
        {
            return Geometry.Combine(this.Body, other.Body, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <summary>
        /// Checks if this object contains parts outside of other
        /// </summary>
        /// <param name="other">the other object, surprisingly</param>
        /// <returns>if sticks out of other</returns>
        public bool SticksOutOf(GameObject other)
        {
            return Geometry.Combine(this.Body, other.Body, GeometryCombineMode.Exclude, null).GetArea() > 0;
        }

        /// <summary>
        /// Applies a transformation and updates Body property
        /// </summary>
        /// <param name="transform">the transformation to apply</param>
        protected void TransformGeometry(Transform transform)
        {
            // clone and set transformation
            Geometry clone = this.Body.Clone();
            clone.Transform = transform;

            // apply transformation
            Geometry transformed = clone.GetFlattenedPathGeometry();

            // replace body with transformed geometry
            this.Body = transformed;
        }
    }
}
