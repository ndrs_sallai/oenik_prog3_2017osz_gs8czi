﻿//-----------------------------------------------------------------------
// <copyright file="Car.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represents the player's car on the screen
    /// </summary>
    public class Car : GameObject
    {
        /// <summary>
        /// Width of the rectangle (body of the car)
        /// </summary>
        private int width = 20;

        /// <summary>
        /// Height of the rectangle (body of the car)
        /// </summary>
        private int height = 30;

        /// <summary>
        /// Current velocity of the car
        /// </summary>
        private double speed = 0;

        /// <summary>
        /// Maximum speed the car can reach
        /// </summary>
        private double maxSpeed = 5;

        /// <summary>
        /// Maximum speed the car can reach
        /// </summary>
        private double minSpeed = -3;

        /// <summary>
        /// Initial angle (this is how we decide to draw the car: nose facing up)
        /// </summary>
        private int angle = 270;

        /// <summary>
        /// Initializes a new instance of the <see cref="Car" /> class.
        /// </summary>
        /// <param name="x">center of body - X</param>
        /// <param name="y">center of body - Y</param>
        /// <param name="angle">initial rotation in degrees</param>
        public Car(double x, double y, int angle)
        {
            Rect rect = new Rect(x - (this.width / 2), y - (this.height / 2), this.width, this.height);
            Geometry bodyBase = new RectangleGeometry(rect);

            this.Body = bodyBase;

            // init rotation
            this.Rotate(angle);
        }

        /// <summary>
        /// Gets speed
        /// </summary>
        public double Speed
        {
            get { return this.speed; }
        }

        /// <summary>
        /// Gets X coordinate of the car's center
        /// </summary>
        public double CenterX
        {
            get
            {
                return (this.Body.Bounds.Left + this.Body.Bounds.Right) / 2;
            }
        }

        /// <summary>
        /// Gets Y coordinate of the car's center
        /// </summary>
        public double CenterY
        {
            get
            {
                return (this.Body.Bounds.Top + this.Body.Bounds.Bottom) / 2;
            }
        }

        /// <summary>
        /// Increases speed by 1
        /// </summary>
        public void Throttle()
        {
            if (this.speed <= (this.maxSpeed - 1))
            {
                this.speed += 1;
            }
        }

        /// <summary>
        /// Decreases speed by 1
        /// </summary>
        public void Brake()
        {
            if (this.speed >= this.minSpeed + 1)
            {
                this.speed -= 1;
            }
        }

        /// <summary>
        /// Sets speed to 0
        /// </summary>
        public void Stop()
        {
            this.speed = 0;
        }

        /// <summary>
        /// Moves the car to its next position
        /// </summary>
        public void UpdatePosition()
        {
            this.Decelerate();

            double dx = this.speed * Math.Cos(this.angle * Math.PI / 180);
            double dy = this.speed * Math.Sin(this.angle * Math.PI / 180);

            this.TransformGeometry(new TranslateTransform(dx, dy));
        }

        /// <summary>
        /// Rotates the car
        /// </summary>
        /// <param name="degrees">in degrees</param>
        public void Rotate(int degrees)
        {
            this.angle += degrees;

            this.TransformGeometry(new RotateTransform(degrees, this.CenterX, this.CenterY));
        }

        /// <summary>
        /// Slowly decreases speed (by 0.1)
        /// </summary>
        protected void Decelerate()
        {
            // TODO: small speed bug. floating point math probably. how to solve?
            if (this.speed > 0)
            {
                this.speed -= 0.1;
            }

            if (this.speed < 0)
            {
                this.speed += 0.1;
            }
        }
    }
}
