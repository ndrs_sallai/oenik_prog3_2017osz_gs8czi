﻿//-----------------------------------------------------------------------
// <copyright file="StartingPosition.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents a starting position
    /// </summary>
    public class StartingPosition
    {
        /// <summary>
        /// Gets or sets X coordinate
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Gets or sets Y coordinate
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// Gets or sets Angle (in degrees)
        /// </summary>
        public int Angle { get; set; }
    }
}
