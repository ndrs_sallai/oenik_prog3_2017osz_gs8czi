﻿//-----------------------------------------------------------------------
// <copyright file="PathBasedRacetrack.cs" company="CompanyName">
//     Absolutely no copyright
// </copyright>
//-----------------------------------------------------------------------
namespace Racecar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represents a path based racetrack
    /// </summary>
    public class PathBasedRacetrack : Racetrack
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PathBasedRacetrack" /> class.
        /// </summary>
        /// <param name="centerX">center x coordinate</param>
        /// <param name="centerY">center y coordinate</param>
        public PathBasedRacetrack(double centerX, double centerY)
        {
            // temporary geometry - seems to be needed (some Readonly state runtime error...)
            Geometry temp = Geometry.Parse("m 50 50 c 20 -9 28 1 20 10 c -8 14 12 18 -20 30 c -14 6 -70 -37 -40 -60 c 24 -14 4 17 40 20");

            // create a clone to represent outer edge of track
            Geometry outer = temp.Clone();
            outer.Transform = new ScaleTransform(10, 10); // scale up
            
            // re-assign 
            temp = Geometry.Parse("m 50 50 c 20 -9 28 1 20 10 c -8 14 12 18 -20 30 c -12 6 -80 -45 -40 -65 c 6 -4 9 13 40 25");

            // clone for inner edge of track
            Geometry inner = temp.Clone();

            // add scale and translation
            TransformGroup tg = new TransformGroup();
            tg.Children.Add(new ScaleTransform(7, 7));
            tg.Children.Add(new TranslateTransform(120, 220)); // arbitrary values...

            // apply transformations to inner edge
            inner.Transform = tg;

            // create racetrack body by extracting inner path from outer path
            Geometry final = Geometry.Combine(outer, inner, GeometryCombineMode.Exclude, null).GetFlattenedPathGeometry();

            // find track center coordinates
            double trackCenterX = final.Bounds.Left + ((final.Bounds.Right - final.Bounds.Left) / 2);
            double trackCenterY = final.Bounds.Top + ((final.Bounds.Bottom - final.Bounds.Top) / 2);

            // move to center
            final.Transform = new TranslateTransform(centerX - trackCenterX, centerY - trackCenterY); // TODO: y calculation

            this.Body = final;
        }

        /// <summary>
        /// Returns the starting position
        /// </summary>
        /// <returns>starting position</returns>
        public override StartingPosition GetStartingPosition()
        {
            return new StartingPosition()
            {
                X = 180,
                Y = 150,
                Angle = 225
            };
        }
    }
}
