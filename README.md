# Programozás 3 féléves feladat - 2017 ősz

## Összefoglaló
A játék egy felülnézetes autóverseny. A játékos egy autót irányít egy versenypályán. Az autót gyorsítani, lassítani, forgatni tudja a billentyűzeten a nyilak segítségével. Az autó egy versenypályán mozog, a pálya szélét érintve azonnal megáll és mindaddig amíg vissza nem tér a pálya területére, csak minimális sebességgel mozoghat. A játék célja minél gyorsabban megtenni egy teljes kört az adott versenypályán.

# Megvalósítás
A játéktér fő grafikai elemei:

	- `Racetrack` class
	- `Car` class

A fentiek a `GameObject` ősosztályból származnak, amely a megjelenítéshez, transzformációkhoz illetve más grafikai elemekkel való összevetéshez tartozó logikát implementál.

A `Racetrack` osztályból leszármaztathatók új osztályok amelyek eltérő módon definiálhatják a pálya testét.
2 ilyen osztályt hoztam létre a projektben: `OctagonalRacetrack` és `PathBasedRacetrack`.
Minden versenypályához tartozik egy `StartingPosition` objektum is, ami a rajtvonal helyét illetve irányát adja meg.

A grafikus megjelenítést a `GameFrameworkElement` osztály biztosítja, ami a `FrameworkElement` leszármazottja.

A `Game` osztály a játéktér elemeit összefogó entitást jelképez illetve felelős a játék állapotának folyamatos frissítéséért (magas szintű irányító). A játék állapotát a 20 millisec-enként frissítjük.

Az autó mozgásának leírást a `Car`˙osztály implementálja. Alapállapotban 0 sebességgel áll a versenypálya rajtvonalánál. A játék során az autó sebessége folyamatosan közelíti a 0-t, vagyis tolatás közben nő, előre haladás közben pedig csökken, így jelképezzük a különböző ellenállásokat, amik mozgás közben hatnak rá.

## Irányítás
Az autót gyorsítani a "fel" gomb folyamatos nyomvatartásával lehet, mindaddig amíg el nem érte a maximális sebességét, annál tovább nem fog gyorsnulni.
Hasonlóan, a tolatáshoz, a "le" gombot kell nyomvatartani, ez esetben is a maximális tolatási sebességig gyorsíthatjuk hátrafelé az autót.
Kanyarodáshoz a "bal" és "jobb" gombokat használjuk, az autó a gombok nyomvatartása alatt folyamatosan fordul a megfelelő irányba, az irányváltoztató gombok felengedését követően pedig egyenesen halad tovább az így beállt irányba.

Az autó irányítását "egyedi" eseménykezelés valósítja meg, ami azt jelenti, hogy nem csak a natív KeyDown és KeyUp események elsülésekor változtatjuk az autó mozgásállapotát. A lenyomott billentyűket folyamatosan számontartva az adott pillanatban épp lenyomott állapotú billentyűket vizsgáljuk állandó időközönként. Erre azért volt szükség, mert a gyári API által nyújtott KeyDown event első és második elsülése között sokkal nagyobb idő telt el, mint az ezt követők között, ez pedig megnehezítette az autó gyorsítását és forgatását a játékos számára.